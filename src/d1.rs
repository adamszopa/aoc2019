use std::fs;

pub fn day1(){
    let d1 = fs::read_to_string("d1.txt").unwrap();

    println!("Day 1 part 1 = {}",day1_part1(d1.clone()));

    println!("Day 1 part 2 = {}",day1_part2(d1));
}

fn day1_part1(s: String) -> u32{
    s.lines().map(|l|l.parse::<u32>().unwrap()/3-2).sum()
}

#[test]
fn day1_part1_test(){
    assert_eq!(day1_part1("12".to_owned()),2);
    assert_eq!(day1_part1("14".to_owned()),2);
    assert_eq!(day1_part1("1969".to_owned()),654);
    assert_eq!(day1_part1("100756".to_owned()),33583);
}
fn day1_part2(s: String)->u32{

    let f = |module: u32|->u32{
        let mut module= module;
        let mut fuel = module;

        while fuel > 8 {
            fuel = (fuel as f32 / 3.0).floor() as u32 - 2;
            module += fuel;
        }
        module
    };

    s.lines().map(|l|l.parse::<u32>().unwrap()/3-2).map(f).sum()

}
#[test]
fn day1_part2_test(){
    assert_eq!(day1_part2("12".to_owned()),2);
    assert_eq!(day1_part2("1969".to_owned()),966);
    assert_eq!(day1_part2("100756".to_owned()),50346);
}
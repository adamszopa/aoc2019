use std::fs;

pub fn day2(){
    let d2_1 = fs::read_to_string("d2.txt").unwrap();
    let d2_2 = d2_1.clone();

    let mut program : Vec<_> = d2_1.split(',').collect::<Vec<_>>().iter().map(|s|s.parse::<u32>().unwrap()).collect();
    let program2 : Vec<_> = d2_2.split(',').collect::<Vec<_>>().iter().map(|s|s.parse::<u32>().unwrap()).collect();

    //print_program(&program);
    program[1] = 12;
    program[2] = 2;
    //print_program(&program);
    day2_part1(&mut program);
    //print_program(&program);

    println!("Day 2 part 1 = {}",program[0]);
    day2_part2(program2);
}

fn print_program(program: &[u32]){
    for i in program{
        print!("{},",i);
    }
    println!();
}

fn day2_part1(input : &mut Vec<u32>){
    let mut position = 0;
    while input[position] != 99{
        let param1 = input[position+1]as usize;
        let param2 = input[position+2]as usize;
        let target = input[position+3]as usize;
        match input[position] {
            1 => input[target] = input[param1] + input[param2],
            2 => input[target] = input[param1] * input[param2],
            99 => break,
            _ => unreachable!(),
        }
        position+=4;
    }
}
#[test]
fn day2_part1_test(){
    let mut input = vec![1,9,10,3,2,3,11,0,99,30,40,50];
    day2_part1(&mut input);
    assert_eq!(input,[3500,9,10,70,2,3,11,0,99,30,40,50]);

    let mut input = vec![1,0,0,0,99];
    day2_part1(&mut input);
    assert_eq!(input,[2,0,0,0,99]);

    let mut input = vec![2,3,0,3,99];
    day2_part1(&mut input);
    assert_eq!(input,[2,3,0,6,99]);

    let mut input = vec![2,4,4,5,99,0];
    day2_part1(&mut input);
    assert_eq!(input,[2,4,4,5,99,9801]);

    let mut input = vec![1,1,1,4,99,5,6,0,99];
    day2_part1(&mut input);
    assert_eq!(input,[30,1,1,4,2,5,6,0,99]);
}

fn day2_part2(program : Vec<u32>){

    for x in 0..100{
        for y in 0..100{
            let mut input = program.clone();
            input[1] = x;
            input[2] = y;
            day2_part1(&mut input);
            if input[0] == 19_690_720{
                println!("Day 2 part 2 = {}",100*x+y);
                return;
            }
        }
    }

    unreachable!();

}
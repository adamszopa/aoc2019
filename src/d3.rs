extern crate ndarray;
extern crate num_traits;

use std::cmp::{max, min};
use std::fs;

use ndarray::s;

use self::ndarray::Array2;

#[derive(Debug, PartialEq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug)]
struct WireSection {
    direction: Direction,
    length: i32,
}

#[derive(Clone, PartialEq, Debug)]
struct Point {
    wire_number: u16,
    distance: i32,
}

impl std::ops::Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            wire_number: self.wire_number + other.wire_number,
            distance: self.distance + other.distance,
        }
    }
}

impl num_traits::identities::Zero for Point {
    fn zero() -> Self {
        Point { wire_number: 0, distance: 0 }
    }
    fn is_zero(&self) -> bool {
        (self.wire_number == 0) && (self.distance == 0)
    }
}

pub fn day3() {
    let (part1, part2) = day3_part1(&fs::read_to_string("d3.txt").unwrap());
    println!("Day 3 part 1 = {}", part1);
    println!("Day 3 part 2 = {}", part2);
}

fn day3_part1(input: &str) -> (i32, i32) {
    let mut wires = vec![];

    for wire_description in input.lines() {
        let wire_section_description = wire_description.split(',').collect::<Vec<_>>();
        let mut parsed = vec![];
        for section in wire_section_description {
            let direction = match section.chars().nth(0).unwrap() {
                'U' => Direction::Up,
                'D' => Direction::Down,
                'L' => Direction::Left,
                'R' => Direction::Right,
                _ => unreachable!(),
            };
            let length = section[1..].parse::<i32>().unwrap();
            parsed.push(WireSection { direction, length });
        }
        wires.push(parsed);
    }

    let mut max_up= 0;
    let mut max_down = 0;
    let mut max_left = 0;
    let mut max_right = 0;

    for wire in &wires{
        let mut current_vertical= 0i32;
        let mut current_horizontal = 0i32;

        let mut local_max_up= 0;
        let mut local_max_down = 0;
        let mut local_max_left = 0;
        let mut local_max_right = 0;

        for section in wire{
            match section.direction {
                Direction::Up => {
                    current_vertical += section.length;
                    local_max_up = max(current_vertical, local_max_up)
                },
                Direction::Down => {
                    current_vertical -= section.length;
                    local_max_down = min(current_vertical, local_max_down)
                },
                Direction::Left => {
                    current_horizontal -= section.length;
                    local_max_left = min(current_horizontal, local_max_left)
                },
                Direction::Right => {
                    current_horizontal += section.length;
                    local_max_right = max(current_horizontal, local_max_right)
                },
            }
        }
        max_up = max(local_max_up,max_up);
        max_down = min(local_max_down,max_down);
        max_left = min(local_max_left,max_left);
        max_right = max(local_max_right,max_right);
    }

    let mut map = Array2::<Point>::zeros(((max_up + -max_down) as usize + 2, (-max_left + max_right) as usize + 2));


    let origin_x = -max_left;
    let origin_y = -max_down;

    let mut closest_distance = 0;
    let mut smallest_length = 0;

    for (wire,wire_number) in wires.iter().zip(1u16..){
        let mut current_x = origin_x;
        let mut current_y = origin_y;

        let mut wire_length = 1;

        for section in wire {
            let direction = match section.direction {
                Direction::Up | Direction::Right => 1,
                Direction::Down | Direction::Left => -1,
            };

            let mut view = if section.direction == Direction::Up || section.direction == Direction::Down {
                let from = min(current_y + if direction == 1 { 1 } else { 0 }, current_y + section.length * direction);
                let to = max(current_y, current_y + section.length * direction + 1);
                map.slice_mut(s![from..to;direction,current_x])
            } else {
                let from = min(current_x + if direction == 1 { 1 } else { 0 }, current_x + section.length * direction);
                let to = max(current_x, current_x + section.length * direction + 1);

                map.slice_mut(s![current_y,from..to;direction])
            };

            for (w, i) in view.iter_mut().zip(1i32..) {
                if w.wire_number != wire_number && w.wire_number != 0 {
                    //calculate distance
                    let distance = if section.direction == Direction::Left || section.direction == Direction::Right { (origin_x - (current_x + i as i32 * direction)).abs() + (origin_y - current_y).abs() } else {
                        (origin_x - current_x).abs() + (origin_y - (current_y + i as i32 * direction)).abs()
                    };

                    if closest_distance == 0 {
                        closest_distance = distance;
                    } else {
                        closest_distance = min(closest_distance, distance);
                    }

                    if smallest_length == 0 {
                        smallest_length = w.distance + wire_length;
                    } else {
                        smallest_length = min(smallest_length, w.distance + wire_length);
                    }
                }

                w.distance = wire_length;
                wire_length += 1;

                w.wire_number = wire_number;
            }

            match section.direction {
                Direction::Left | Direction::Right => current_x += section.length * direction,
                Direction::Up | Direction::Down => current_y += section.length * direction,
            };
        }
    }

    (closest_distance, smallest_length)

}

#[test]
fn day3_part1_test() {
    assert_eq!(day3_part1("R8,U5,L5,D3\nU7,R6,D4,L4"), (6, 30));
    assert_eq!(day3_part1("R75,D30,R83,U83,L12,D49,R71,U7,L72\nU62,R66,U55,R34,D71,R55,D58,R83"), (159, 610));
    assert_eq!(day3_part1("R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\nU98,R91,D20,R16,D67,R40,U7,R15,U6,R7"), (135, 410));
}













pub fn day4() {
    let from = 183564;
    let to = 657474;

    println!("Day 4 part 1 = {}", (from..=to).filter(|i| check_part1(i)).count());
    println!("Day 4 part 2 = {}", (from..=to).filter(|i| check_part2(i)).count());

    /*
    for i in from..=to{
        if che
    }    */
}


#[test]
fn check_test() {
    assert_eq!(check_part1(&1223), true);
    assert_eq!(check_part1(&1233), true);
    assert_eq!(check_part1(&1123), true);
    assert_eq!(check_part1(&1234), false);
    assert_eq!(check_part1(&1221), false);
    assert_eq!(check_part2(&112233), true);
    assert_eq!(check_part2(&123444), false);
    assert_eq!(check_part2(&111122), true);
    assert_eq!(check_part2(&112333), true);
}

fn check_part1(number: &i32) -> bool {
    let digits: Vec<_> = number.to_string().chars().map(|d| d.to_digit(10).unwrap()).collect();

    let mut last_digit = digits[0];

    let mut has_double_digit = false;

    for i in 1..digits.len() {
        if digits[i] == last_digit {
            has_double_digit = true;
        }
        if last_digit > digits[i] {
            return false;
        }
        last_digit = digits[i];
    }

    return has_double_digit
}


fn check_part2(number: &i32) -> bool {
    let digits: Vec<_> = number.to_string().chars().map(|d| d.to_digit(10).unwrap()).collect();

    let mut last_digit = digits[0];

    let mut has_double_digit = false;
    let mut doubled = 10;

    for i in 1..digits.len() {
        if digits[i] == last_digit {
            if !has_double_digit {
                doubled = last_digit;
            }
            has_double_digit = true;
            if i > 1 && digits[i - 2] == digits[i] && doubled == digits[i] {
                has_double_digit = false;
            }
        }
        if last_digit > digits[i] {
            return false;
        }
        last_digit = digits[i];
    }

    return has_double_digit
}
use std::fs;

pub fn day5() {
    let d5_1 = fs::read_to_string("d5.txt").unwrap();
    let d5_2 = d5_1.clone();

    let mut program: Vec<_> = d5_1.split(',').collect::<Vec<_>>().iter().map(|s| s.parse::<i32>().unwrap()).collect();
    let mut program2: Vec<_> = d5_2.split(',').collect::<Vec<_>>().iter().map(|s| s.parse::<i32>().unwrap()).collect();

    let answer = day5_part1(&mut program, &mut vec![1]);
    dbg!(&answer);
    println!("Day 5 part 1 = {}", answer.last().unwrap());


    let answer = day5_part1(&mut program2, &mut vec![5]);
    println!("Day 5 part 2 = {}", answer.last().unwrap());
}


fn day5_part1(program: &mut Vec<i32>, input: &mut Vec<i32>) -> Vec<i32> {
    let mut position = 0;
    let mut output = vec![];
    loop {
        let opcode = program[position] % 100;
        let immediate_mode_param1 = program[position] % 1000 > 99;
        let immediate_mode_param2 = program[position] % 10000 > 999;
        let immediate_mode_target = program[position] % 100000 > 9999;

        match opcode {
            1 => {
                let param1 = if immediate_mode_param1 { position + 1 } else { program[position + 1] as usize };
                let param2 = if immediate_mode_param2 { position + 2 } else { program[position + 2] as usize };
                let target = if immediate_mode_target { position + 3 } else { program[position + 3] as usize };
                program[target] = program[param1] + program[param2];
                position += 4;
            }
            2 => {
                let param1 = if immediate_mode_param1 { position + 1 } else { program[position + 1] as usize };
                let param2 = if immediate_mode_param2 { position + 2 } else { program[position + 2] as usize };
                let target = if immediate_mode_target { position + 3 } else { program[position + 3] as usize };
                program[target] = program[param1] * program[param2];
                position += 4;
            }
            3 => {
                let param1 = if immediate_mode_param1 { position + 1 } else { program[position + 1] as usize };
                program[param1] = input.pop().unwrap();
                position += 2;
            }
            4 => {
                let param1 = if immediate_mode_param1 { position + 1 } else { program[position + 1] as usize };
                output.push(program[param1]);
                position += 2;
            }
            5 => {
                let param1 = if immediate_mode_param1 { position + 1 } else { program[position + 1] as usize };
                let param2 = if immediate_mode_param2 { position + 2 } else { program[position + 2] as usize };

                if program[param1] != 0 {
                    //position = param2;

                    position = program[param2] as usize;
                } else {
                    position += 3;
                }
            }
            6 => {
                let param1 = if immediate_mode_param1 { position + 1 } else { program[position + 1] as usize };
                let param2 = if immediate_mode_param2 { position + 2 } else { program[position + 2] as usize };

                if program[param1] == 0 {
                    //position = param2;

                    position = program[param2] as usize;
                } else {
                    position += 3;
                }
            }
            7 => {
                let param1 = if immediate_mode_param1 { position + 1 } else { program[position + 1] as usize };
                let param2 = if immediate_mode_param2 { position + 2 } else { program[position + 2] as usize };
                let target = if immediate_mode_target { position + 3 } else { program[position + 3] as usize };
                program[target] = if program[param1] < program[param2] { 1 } else { 0 };
                //program[target] = if param1 < param2 {1} else {0};
                position += 4;
            }
            8 => {
                let param1 = if immediate_mode_param1 { position + 1 } else { program[position + 1] as usize };
                let param2 = if immediate_mode_param2 { position + 2 } else { program[position + 2] as usize };
                let target = if immediate_mode_target { position + 3 } else { program[position + 3] as usize };
                program[target] = if program[param1] == program[param2] { 1 } else { 0 };
                //program[target] = if param1 == param2 {1} else {0};
                position += 4;
            }
            99 => break,
            _ => unreachable!(),
        }
    }
    output
}

#[test]
fn day5_part1_test() {
    let mut input = vec![1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50];
    day5_part1(&mut input, &mut vec![]);
    assert_eq!(input, [3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50]);

    let mut input = vec![1, 0, 0, 0, 99];
    day5_part1(&mut input, &mut vec![]);
    assert_eq!(input, [2, 0, 0, 0, 99]);

    let mut input = vec![2, 3, 0, 3, 99];
    day5_part1(&mut input, &mut vec![]);
    assert_eq!(input, [2, 3, 0, 6, 99]);

    let mut input = vec![2, 4, 4, 5, 99, 0];
    day5_part1(&mut input, &mut vec![]);
    assert_eq!(input, [2, 4, 4, 5, 99, 9801]);

    let mut input = vec![1, 1, 1, 4, 99, 5, 6, 0, 99];
    day5_part1(&mut input, &mut vec![]);
    assert_eq!(input, [30, 1, 1, 4, 2, 5, 6, 0, 99]);

    let mut input = vec![3, 0, 4, 0, 99];
    assert_eq!(day5_part1(&mut input, &mut vec![-1]), vec![-1]);

    let mut input = vec![1002, 4, 3, 4, 33];
    day5_part1(&mut input, &mut vec![]);
}

#[test]
fn day5_part2_test() {
    let mut input = vec![3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8];
    assert_eq!(day5_part1(&mut input, &mut vec![8]), vec![1]);
    let mut input = vec![3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8];
    assert_eq!(day5_part1(&mut input, &mut vec![7]), vec![0]);
    let mut input = vec![3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8];
    assert_eq!(day5_part1(&mut input, &mut vec![7]), vec![1]);
    let mut input = vec![3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8];
    assert_eq!(day5_part1(&mut input, &mut vec![8]), vec![0]);


    let mut input = vec![3, 3, 1108, -1, 8, 3, 4, 3, 99];
    assert_eq!(day5_part1(&mut input, &mut vec![8]), vec![1]);
    let mut input = vec![3, 3, 1108, -1, 8, 3, 4, 3, 99];
    assert_eq!(day5_part1(&mut input, &mut vec![7]), vec![0]);
    let mut input = vec![3, 3, 1107, -1, 8, 3, 4, 3, 99];
    assert_eq!(day5_part1(&mut input, &mut vec![7]), vec![1]);
    let mut input = vec![3, 3, 1107, -1, 8, 3, 4, 3, 99];
    assert_eq!(day5_part1(&mut input, &mut vec![8]), vec![0]);


    let mut input = vec![3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9];
    assert_eq!(day5_part1(&mut input, &mut vec![0]), vec![0]);
    let mut input = vec![3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9];
    assert_eq!(day5_part1(&mut input, &mut vec![5]), vec![1]);
    let mut input = vec![3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9];
    assert_eq!(day5_part1(&mut input, &mut vec![-5]), vec![1]);

    let mut input = vec![3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1];
    assert_eq!(day5_part1(&mut input, &mut vec![0]), vec![0]);
    let mut input = vec![3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1];
    assert_eq!(day5_part1(&mut input, &mut vec![5]), vec![1]);
    let mut input = vec![3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1];
    assert_eq!(day5_part1(&mut input, &mut vec![-5]), vec![1]);

    let mut input = vec![3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31,
                         1106, 0, 36, 98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104,
                         999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99
    ];
    assert_eq!(day5_part1(&mut input, &mut vec![5]), vec![999]);
    let mut input = vec![3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31,
                         1106, 0, 36, 98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104,
                         999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99
    ];
    assert_eq!(day5_part1(&mut input, &mut vec![8]), vec![1000]);
    let mut input = vec![3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31,
                         1106, 0, 36, 98, 0, 0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104,
                         999, 1105, 1, 46, 1101, 1000, 1, 20, 4, 20, 1105, 1, 46, 98, 99
    ];
    assert_eq!(day5_part1(&mut input, &mut vec![10]), vec![1001]);
}